#Exercise_1
#In the function exercise_one on first_part.src module:
#print every number between 1 and 100 as follows:

#For every multiple of 3 print "Three".
#For every multiple of 5 print "Five".
#And for every multiple of both 3 and 5 print "ThreeFive"


def Exercise_1():
     for number in range(1, 101):
         if number % 3 == 0 and number % 5 == 0:
             print("ThreeFive")
         elif number % 3 == 0:
             print("Three")
         elif number % 5 == 0:
             print("Five")
         else:
             print(number)

Exercise_1()




#Exercise 2



def is_colorful(number):
    digits = [int(n) for n in str(number)]
    products = set()

    for i in range(len(digits)):
        for j in range(i + 1, len(digits) + 1):
            subset = digits[i:j]
            product = 1
            for k in subset:
                product =product*k

            if product in products:
                return False
            else:
                products.add(product)

    return True


print(is_colorful(263))   # Output: True
print(is_colorful(236))   # Output: False
print(is_colorful(2532))  # Output: False



#Exercise 3


def calculate(lst):
    total = 0

    for item in lst:
        if isinstance(item, str):
            try:
                num = int(item)
                total += num
            except ValueError:
                continue

    return total if total != 0 else False

print(calculate(['4', '3', '-2']))            # Output: 5
print(calculate([453]))                       # Output: False
print(calculate(['nothing', 3, '8', 2, '1']))  # Output: 9
print(calculate('54'))                         # Output: False




# Exercise 4

def anagrams(word, word_list):
    result = []
    for i in word_list:
        if len(i) == len(word) and sorted(i) == sorted(word):
            result.append(i)

    return result


print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))  # Output: ['aabb', 'bbaa']
print(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))  # Output: ['carer', 'racer']
print(anagrams('laser', ['lazing', 'lazy', 'lacer']))  # Output: []









