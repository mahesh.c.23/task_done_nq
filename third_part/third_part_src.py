
## Exercise 1

import requests

def http_request(is_admin=1, change_user_agent=False):
    url = "https://httpbin.org/anything"
    params = {'isadmin': is_admin}

    headers = {'User-Agent': 'YourCustomUserAgent'} if change_user_agent else {}

    response = requests.post(url, params=params, headers=headers)

    if response.status_code == 200:
        return response.text
    else:
        return f"Error: {response.status_code}"

# Example usage:
response_body = http_request()
print("Response with default User-Agent:")
print(response_body)

response_body_custom = http_request(change_user_agent=True)
print("\nResponse with custom User-Agent:")
print(response_body_custom)




## Excercise 2


import json
import gzip
import csv

class ProductHandler:
    def __init__(self, file_path='/home/mahi/Downloads/test_python_scraping-master/third_part/data/data.json.gz'):
        self.file_path = file_path
        self.products = []

    def load_data(self):
        try:
            with gzip.open(self.file_path, 'rt', encoding='utf-8') as file:
                data = json.load(file)
                self.products=data.get('Bundles',[])
        except FileNotFoundError:
            print("Error: Data file not found.")
        except json.JSONDecodeError:
            print("Error: Unable to decode JSON data.")

    def print_and_save_products(self):
        if not self.products:
            print("Error: No products found.")
            return

        available_products = []

        for product in self.products:
            if product.get('Product'):
            
                product_info = product['Product'][0]
                product_id=product_info.get('Stockcode')
                product_name = product_info.get('Name', 'Unknown')
                product_price = round(float(product_info.get('Price', 0)), 1)
                product_available = product_info.get('IsAvailable', False)

                truncated_name = product_name[:30] if len(product_name) > 30 else product_name

                if product_available:
                    available_products.append({'name': truncated_name, 'price': product_price})
                    print(f"You can buy {truncated_name} at our store at {product_price:.1f}")
                else:
                    print(f"Product {product_id}: {truncated_name} is unavailable.")
            

            elif product.get('Products'):
                product_info = product['Products'][0]
                product_id=product_info.get('Stockcode')
                product_name = product_info.get('Name', 'Unknown')
                product_price = round(float(product_info.get('Price', 0) or 0), 1)
                product_available = product_info.get('IsAvailable', False)

                truncated_name = product_name[:30] if len(product_name) > 30 else product_name

                if product_available:
                    available_products.append({'name': truncated_name, 'price': product_price})
                    print(f"You can buy {truncated_name} at our store at {product_price:.1f}")
                else:
                    print(f"Product {product_id}: {truncated_name} is unavailable.")
            else:
                continue


        self.save_to_csv(available_products)

    def save_to_csv(self, available_products):
        if not available_products:
            return

        csv_file_path = 'available_products.csv'

        with open(csv_file_path, 'w', newline='') as csv_file:
            fieldnames = ['name', 'price']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            for product in available_products:
                writer.writerow(product)

        print(f"\nAvailable products saved to {csv_file_path}")

# Example usage:
if __name__ == "__main__":
    product_handler = ProductHandler()
    product_handler.load_data()
    product_handler.print_and_save_products()
